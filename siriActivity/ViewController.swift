//
//  ViewController.swift
//  siriActivity
//
//  Created by jatin verma on 2019-10-29.
//  Copyright © 2019 jatin verma. All rights reserved.
//

import UIKit
import Particle_SDK
import Speech
class ViewController: UIViewController , SFSpeechRecognizerDelegate{

    
    
    // mandatory variables
 let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    // outlets
    @IBOutlet weak var TextLabel: UILabel!
    
    
    @IBOutlet weak var startButton: UIButton!
    
    @IBAction func startAction(_ sender: Any) {
        
       recordAndRecognizeSpeech()
    }
    // MARK: User variables
    let USERNAME = "jatin_verma@outlook.com"
    let PASSWORD = "kaur1234"
    
    // MARK: Device
    
    // Antonio's device
    let DEVICE_ID = "3f003b001047363333343437"
    var myPhoton : ParticleDevice?
    
    override func viewDidLoad() {
           super.viewDidLoad()
          
           // 1. Initialize the SDK
           ParticleCloud.init()
           //Jatin
    
           // 2. Login to your account
           ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
               if (error != nil) {
                   // Something went wrong!
                   print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                   // Print out more detailed information
                   print(error?.localizedDescription)
               }
               else {
                   print("Login success!")

                   // try to get the device
                   self.getDeviceFromCloud()

               }
           } // end login
           
          // self.requestSpeechAuthorization()
           
       }
    // MARK: Get Device from Cloud
    // Gets the device from the Particle Cloud
    // and sets the global device variable
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
              //  self.subscribeToParticleEvents()
            }
            
        } // end getDevice()
    }
    // function required for Speech Record and Recognition
    func recordAndRecognizeSpeech(){
        guard let node: AVAudioNode = audioEngine.inputNode  else {
            return
        }
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat){ buffer, _ in
            self.request.append(buffer)
        }
        
        
        audioEngine.prepare()
        do{
            try audioEngine.start()
        }catch{
            return print(error)
        }
        
        
        guard let recognizer = SFSpeechRecognizer() else {
            return
        }
        if !recognizer.isAvailable{
            return
        }
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: {result, error in
            if let result = result{

                let CommandString =  result.bestTranscription.formattedString
                self.TextLabel.text = CommandString
                if (CommandString == "Music"){
                    self.turnParticleMusic()
                }
                else if(CommandString == "Green"){
                    self.turnParticleGreen()
                }
            }
            else if let error = error {
                print(error)
            }
            
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func turnParticleGreen() {
        
        print("Pressed the change lights button")
        
        let parameters = ["green"]
        var task = myPhoton!.callFunction("command", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
    
    
    func turnParticleMusic() {
        
        print("Pressed the change lights button")
        
        let parameters = ["music"]
        var task = myPhoton!.callFunction("command", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn on music")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }

        
    }
    
}

